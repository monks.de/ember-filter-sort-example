import EmberObject, { computed } from '@ember/object';
import { A } from '@ember/array';
import Controller from '@ember/controller';
import FilsorMixin from 'ember-filter-sort/mixins/filsor-mixin';

export default Controller.extend(FilsorMixin, {

  'filsorDataKey': "people",

  init: function () {
    this.send('filsorSort', 'float', 'entfernung');
    this._super(...arguments);
  },

  people: A([

    EmberObject.create({
      age:       53,
      city:      "Foo",
      favorite:  true,
      name:      "Theodor W. Adorno",
      zip:       "12345",
      status:    "green"
    }),

    EmberObject.create({
      age:       9000,
      city:      "Foobar",
      favorite:  true,
      name:      "Karl Marx",
      zip:       "11234",
      status:    "red"
    }),

    EmberObject.create({
      age:       500,
      city:      "Weimar",
      favorite:  false,
      name:      "Rosa Luxemburg",
      zip:       "11223",
      status:    "green"
    }),

    EmberObject.create({
      age:       1234,
      city:      "Baz",
      favorite:  true,
      name:      "Foo Bar",
      zip:       "34567",
      status:    "yellow"
    }),

    EmberObject.create({
      age:       12,
      city:      "Dolor",
      favorite:  false,
      name:      "Lorem Ipsum",
      zip:       "00001",
      status:    "yellow"
    }),

    EmberObject.create({
      age:       30,
      city:      "Gotham",
      favorite:  false,
      name:      "Batman",
      zip:       "99999",
      status:    "green"
    }),

    EmberObject.create({
      age:       1,
      city:      "City",
      favorite:  false,
      name:      "Name",
      zip:       "12345",
      status:    "green"
    })

  ]),

  'splice-red-yellow-green': computed(
    'filsorFilteredStatus',
    function () {
      return ['red', 'yellow', 'green'].splice(
        this.get('filsorFilteredStatus')
      ).reverse().join(', ');
    }
  )

});
