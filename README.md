This is an examplary [Ember](https://emberjs.com/)-application to demonstrate
the usage of the
[ember-filter-sort](https://gitlab.com/monks.de/ember-filter-sort) addon.

# Read the Source Code

This application is very simple. It only consists of the index controller. So,
take a look at its

* [Javascript
  module](https://gitlab.com/monks.de/ember-filter-sort-example/blob/master/app/controllers/index.js);
  and its

* [Handlebars
  template](https://gitlab.com/monks.de/ember-filter-sort-example/blob/master/app/templates/index.hbs).

# View the Application in Action

You can run and view it easily as follows:

```
git clone https://gitlab.com/monks.de/ember-filter-sort-example
cd ember-filter-sort-example
npm install
ember serve
```
